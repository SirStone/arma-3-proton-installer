# Arma 3 Proton Installer

This is the ArmaOnUnix Proton installer, this installer automates the instructions listed on the ArmaOnUnix Wiki as well as installing fixes.

Run install.py if you haven't had the game run in proton yet then run install-teamspeak.py to install teamspeak and the needed teamspeak plugin TFAR (you need to be subbed to TFAR to install teamspeak)

if you have other issues you might need to run the installer again from time to time


#### WARNING:
This project is no longer maintained and might harm system files, **do not use**, this repo remains here for historical resource reasons.

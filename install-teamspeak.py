#!/bin/python3
import os, subprocess, re
from sys import argv
home = os.path.expanduser("~")
libraries = []
if os.path.isfile(home+"/.local/share/Steam/steamapps/libraryfolders.vdf"):
    steam_library = str(home+"/.local/share/Steam/steamapps/libraryfolders.vdf")
    libraries.append(home+"/.local/share/Steam")
elif os.path.isfile(home+"/.steam/steam/steamapps/libraryfolders.vdf"):
    steam_library = str(home+"/.steam/steam/steamapps/libraryfolders.vdf")
    libraries.append(home+"/.steam/steam")

with open(steam_library, 'r') as libvdf:
    libraries += re.findall(r'(?:\/[\w]+\/.+[\w]+)+', libvdf.read())
    proton_dir = ""
    steamapps_dir = ""

    for library in libraries:
        if os.path.isdir(library+"/steamapps/common/Arma 3"):
            steamapps_dir = library+"/steamapps"
        if os.path.isdir(library+"/steamapps/common/Proton 4.2/"):
            proton_dir = library+"/steamapps/common/Proton 4.2"
    if not os.path.isdir(steamapps_dir+"/common/Arma 3") or steamapps_dir == "":
        print("ERROR: Arma 3 folder not found")
        exit(1)
    if not os.path.isdir(proton_dir) or proton_dir == "":
        print("ERROR: Proton 4.2 not found")
        exit(1)


tfarModule = steamapps_dir+"/workshop/content/107410/620019431/teamspeak/task_force_radio.ts3_plugin"
proton = proton_dir+"/dist/bin/wine64"
try:
    script, teamspeakExeDir = argv
except:
    print("\nERROR: This script needs an argument\nPlease provide the path to the teamspeak installer. ex: ./install-teamspeak.py ~/Downloads/TeamSpeak3-Client-win64-3.2.5.exe\n")

if not os.path.isfile(teamspeakExeDir):
    print("ERROR: "+teamspeakExeDir+" File Not found")
    exit(1)

os.environ["WINEPREFIX"] = steamapps_dir+"/compatdata/107410/pfx"
os.environ["WINEESYNC"] = "1"

if not os.path.isdir(steamapps_dir+"/compatdata/107410/pfx/drive_c/users/steamuser/Local\ Settings/Application\ Data/Teamspeak\ 3\ Client"):
    print("Installing Teamspeak 3...")
    subprocess.call([proton, teamspeakExeDir])
    print("Teamspeak 3 finished installing")

packageInstaller = steamapps_dir+"/compatdata/107410/pfx/drive_c/users/steamuser/Local Settings/Application Data/TeamSpeak 3 Client/package_inst.exe"

teamspeakClient = steamapps_dir+"/compatdata/107410/pfx/drive_c/users/steamuser/Local Settings/Application Data/TeamSpeak 3 Client/ts3client_win64.exe"

input("\nThis scirpt will start Teamspeak to initialize it, please close Teamspeak after choosing a nickname\n Press enter to continue\n")
subprocess.call([proton, teamspeakClient])
input("\nPress Enter to install the TFAR plugin\n")
print("Installing TFAR plugin...")
subprocess.call([proton, packageInstaller, tfarModule])

print("Teamspeak 3 has finished installing")
exit(0)

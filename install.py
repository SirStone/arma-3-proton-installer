#!/bin/python
import os, platform, subprocess, re, tarfile
def main():
    print ("Setting up...")
    def find_libraries():
        home = os.path.expanduser("~")
        libraries = []
        if os.path.isfile(home+"/.steam/steam/steamapps/libraryfolders.vdf"):
            steam_library = str(home+"/.steam/steam/steamapps/libraryfolders.vdf")
            libraries.append(home+"/.steam/steam")
        elif os.path.isfile(home+"/.local/share/Steam/steamapps/libraryfolders.vdf"):
            steam_library = str(home+"/.local/share/Steam/steamapps/libraryfolders.vdf")
            libraries.append(home+"/.local/share/Steam")

        with open(steam_library, 'r') as libvdf:
            libraries += re.findall(r'(?:\/[\w]+\/.+[\w]+)+', libvdf.read())
            for library in libraries:
                if os.path.isdir(library+"/steamapps/common/Arma 3"):
                    steamapps_dir = library+"/steamapps"
                if os.path.isdir(library+"/steamapps/common/Proton 4.2"):
                    proton_dir = library+"/steamapps/common/Proton 4.2"
            if not os.path.isdir(steamapps_dir+"/common/Arma 3"):
                print("ERROR: Arma 3 folder not found")
                exit(1)
            if not os.path.isdir(proton_dir):
                print("Warning: Proton 4.2 not found")
                proton_dir = install_proton(base_dir, steamapps_dir)

        return steamapps_dir, proton_dir


    def detect(distro):
        linuxVer = platform.release()
        linuxVer = linuxVer.lower()

        if platform.system() == "Linux":
            if distro.lower() in linuxVer:
                usrInput = input(distro+" was detected, is this correct?: (Y/N)\n")
                if usrInput.lower() == "y":
                    print ("Installing and setting up Arma 3 Proton...")
                    return True
                else:
                    print ("Exiting installer...")
                    exit(1)
            else:
                return False
        else:
            print (platform.system()+" is not supported by this installer, exiting...")
            exit(1)

    def install(distro, base_dir, steamapps_dir, proton_dir):

        def rename_exes(base_dir, steamapps_dir):
            print ("Renaming Arma 3 exe's...")
            arma3_dir = str(steamapps_dir+"/common/Arma 3")
            if os.path.isfile(arma3_dir+"/arma3launcher.exe") and os.path.isfile(arma3_dir+"/arma3_x64.exe"):
                os.system("mv "+steamapps_dir+"/common/Arma\ 3"+"/arma3launcher.exe "+steamapps_dir+"/common/Arma\ 3"+"/arma3launcher.exe.backup")
                os.system("mv "+steamapps_dir+"/common/Arma\ 3"+"/arma3_x64.exe "+steamapps_dir+"/common/Arma\ 3"+"/arma3launcher.exe")
            else:
                print ("Warning: arma3launcher.exe or arma3_x64.exe not found, did you already rename it?")

        print ("Installing...")
        if distro == "Debian" or distro == "Ubuntu":
            print ("Installing dependencies...")
            if os.system("sudo apt install mesa-vulkan-drivers vulkan-utils") != 0:
                print ("An error occurred, Aborting install...")
                exit(1) 
            setup_prefix(base_dir, steamapps_dir, proton_dir)
            rename_exes(base_dir, steamapps_dir)

            print ("Install complete! enjoy your game.")
            exit(0)

        elif distro == "Arch":
            setup_prefix(base_dir, steamapps_dir, proton_dir)
            rename_exes(base_dir, steamapps_dir)

            print ("Install complete! you used arch btw")
            exit(0)

        elif distro == "Fedora":
            setup_prefix(base_dir, steamapps_dir, proton_dir)
            rename_exes(base_dir, steamapps_dir)

            print ("Install complete! *tips fedora*.")
            exit(0)

        else:
            setup_prefix(base_dir, steamapps_dir, proton_dir)
            rename_exes(base_dir, steamapps_dir)

            print ("Install complete! *insert distro joke here*")

    def setup_prefix(base_dir, steamapps_dir, proton_dir):
        proton_dir_cli_safe = proton_dir.replace(" ", "\\ ")

        if not os.path.isdir(base_dir+"/Resources/winetricks"):
            print ("\nDownloading winetricks...\n")
            os.mkdir(base_dir+"/Resources/winetricks")
            os.system('wget -P '+base_dir+'/Resources/winetricks/ "https://raw.githubusercontent.com/Winetricks/winetricks/master/src/winetricks"')
            os.chmod(base_dir+'/Resources/winetricks/winetricks', 0o775)

        print ("\nSetting up prefix...\n")

        if not os.path.isfile(steamapps_dir+"/compatdata/107410/pfx/user.reg"):
                input("\nThe prefix has not yet been initialized, this script will now start Arma 3, you might get errors but you can ignore them.\nPress enter to continue...")
                #steam = "~/.local/share/Steam/steam.sh"
                print("Waiting for Arma 3 to initialize...")
                subprocess.call(['steam', 'steam://run/107410'])
                while True:
                    if os.path.isfile(steamapps_dir+"/compatdata/107410/pfx/user.reg"):
                        input("Arma 3 has finished initializing, please wait until it finishes first time setup then press enter to continue...")
                        break
                    os.system("sleep 5")



        os.system("cp "+base_dir+"/Resources/imagehlp.dll.so "+proton_dir_cli_safe+"/dist/lib64/wine/imagehlp.dll.so")
        os.environ["WINEDLLPATH"] = str(proton_dir_cli_safe+"/dist/lib64/wine:"+proton_dir_cli_safe+"/dist/lib/wine")
        os.environ["PATH"] = str(proton_dir_cli_safe+"/dist/bin/:"+proton_dir_cli_safe+"/dist/lib/:"+proton_dir_cli_safe+"/dist/lib64/:/sbin:/bin:/usr/sbin:/usr/bin:/usr/local/bin:/snap/bin")
        os.environ["WINEPREFIX"] = str(steamapps_dir+"/compatdata/107410/pfx/")

        winetricks = str(base_dir+"/Resources/winetricks/winetricks")

        # set override dlls
        # refuses to work if the extra indent isn't there, no idea why
        def override_dll(dllName, loadType, insertPoint):
                with open(steamapps_dir+"/compatdata/107410/pfx/user.reg", 'r+') as f:
                    text = f.read()
                    while True:
                        with open(steamapps_dir+"/compatdata/107410/pfx/user.reg", 'r+') as nf:
                            if dllName in nf.read():
                                break
                            insertPoint = "\""+insertPoint+"\"=\"native,builtin\""
                            dllOverride = "\""+dllName+"\"=\""+loadType+"\""
                            text = re.sub(insertPoint, insertPoint+'\n'+dllOverride, text)
                            nf.seek(0)
                            nf.truncate()
                            nf.write(text)
                            print ("Trying to override "+dllName+"...")
                            os.system("sleep 2")
                        with open(steamapps_dir+"/compatdata/107410/pfx/user.reg", 'r+') as cf:
                            if dllName in cf.read():
                                break
                            else:
                                continue

        override_dll("imagehlp", "native,builtin", "concrt140")
        override_dll("api-ms-win-crt-private-l1-1-0", "native,builtin", "api-ms-win-crt-math-l1-1-0")

        # need to call winetricks twice to fix some bug with user.reg, possible fix
        # is to run override dll before the winetricks process:::: FIXED
        subprocess.call([winetricks, "d3dcompiler_43", "d3dx10_43", "d3dx11_43", "xact_x64"])


    def steam_cmd(base_dir, steamapps_dir, app_name, app_id, force_windows):
        if not os.path.isdir(base_dir+"/Resources/steamcmd"):
            print ("\nDownloading steamcmd...\n")
            os.mkdir(base_dir+"/Resources/steamcmd")
            os.system('wget "https://steamcdn-a.akamaihd.net/client/installer/steamcmd_linux.tar.gz" && tar -xf steamcmd_linux.tar.gz -C '+base_dir+'/Resources/steamcmd')

        steam_user = input("Please enter your Steam username: ")
        steamcmd = base_dir+"/Resources/steamcmd/steamcmd.sh"
        login = "+login "+steam_user
        install_dir = str('+force_install_dir "'+steamapps_dir+"/common"+'/'+app_name+'"')
        app_update = "+app_update "+app_id
        if force_windows == True:
            platform_type = "+@sSteamCmdForcePlatformType windows"
        else:
            platform_type = "+@sSteamCmdForcePlatformType linux"
        subprocess.call([steamcmd, platform_type, login, install_dir, app_update, "validate", "+quit"])
        return

    def install_proton(base_dir, steamapps_dir):
        print ("\nInstalling Proton 4.2...\n")
        steam_cmd(base_dir, steamapps_dir, "Proton 4.2", "1054830", False)
        print ("\nExtracting dist...\n")
        proton_dir = steamapps_dir+"/common/Proton 4.2"
        proton_dir_cli_safe = proton_dir.replace(" ", "\\ ")
        tar = tarfile.open(proton_dir+"/proton_dist.tar.gz", mode="r:gz")
        tar.extractall(path=proton_dir+"/dist")
        tar.close()
        os.system("cp "+proton_dir_cli_safe+"/version "+proton_dir_cli_safe+"/dist/")
        os.system("cp -r "+proton_dir_cli_safe+"/steamapps "+steamapps_dir+"/../")
        return proton_dir

    # MAIN
    base_dir = os.getcwd()

    print ("Finding Steam library locations...")
    steamapps_dir, proton_dir = find_libraries()
    print ("Detecting OS and distro...")
    for distro in ["Debian", "Ubuntu", "PopOS", "Fedora", "Arch", "Void"]:
        if detect(distro):
            install(distro, base_dir, steamapps_dir, proton_dir)
            break
    else:
        user_input = input(platform.release()+" Is not yet supported, continue anyway? (Y/N)\n")
        if user_input.lower() != "n":
            install("Unknown", base_dir, steamapps_dir, proton_dir)
        else:
            exit(0)

if __name__ == '__main__':
    main()

# vim: set syntax=python:
